<?php
/**
 * Plugin Name: Frieda Roles (Super CB Manager) for CommonsBooking2
 * Description:
 * Author: Nils Larsen
 * Author URI: https://www.friedafriedrich.de
 */



function friedaroles_create_super_cb_manager()
{
    $cb_manager = get_role('cb_manager');
    $capabilities = $cb_manager->capabilities;

    // add post-edit capabilities so super CB manager can edit cb_item_owner
    $capabilities['edit_posts'] = true;
    $capabilities['publish_posts'] = true;
    $capabilities['delete_posts'] = true;
    $capabilities['create_posts'] = true;
    $capabilities['edit_others_posts'] = true;
    $capabilities['edit_published_posts'] = true;
    $capabilities['delete_others_posts'] = true;
    $capabilities['delete_published_posts'] = true;
    $capabilities['read_private_posts'] = true;

    add_role('super_cb_manager', 'Super CB Mananger', $capabilities);
}

register_activation_hook(__FILE__, 'friedaroles_create_super_cb_manager');


function friedaroles_delete_super_cb_manager()
{
    remove_role('super_cb_manager');
}

register_deactivation_hook(__FILE__, 'friedaroles_delete_super_cb_manager');



function friedaroles_add_super_cb_as_commonsbooking_admin($adminRoles) {
    $adminRoles[] = 'super_cb_manager';

    return $adminRoles;
}

add_filter('commonsbooking_admin_roles', 'friedaroles_add_super_cb_as_commonsbooking_admin');

?>